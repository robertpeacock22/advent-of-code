defmodule Y2019.Day05 do
  @instructions [3,225,1,225,6,6,1100,1,238,225,104,0,1002,36,25,224,1001,224,-2100,224,4,224,1002,223,8,223,101,1,224,224,1,223,224,223,1102,31,84,225,1102,29,77,225,1,176,188,224,101,-42,224,224,4,224,102,8,223,223,101,3,224,224,1,223,224,223,2,196,183,224,1001,224,-990,224,4,224,1002,223,8,223,101,7,224,224,1,224,223,223,102,14,40,224,101,-1078,224,224,4,224,1002,223,8,223,1001,224,2,224,1,224,223,223,1001,180,64,224,101,-128,224,224,4,224,102,8,223,223,101,3,224,224,1,223,224,223,1102,24,17,224,1001,224,-408,224,4,224,1002,223,8,223,101,2,224,224,1,223,224,223,1101,9,66,224,1001,224,-75,224,4,224,1002,223,8,223,1001,224,6,224,1,223,224,223,1102,18,33,225,1101,57,64,225,1102,45,11,225,1101,45,9,225,1101,11,34,225,1102,59,22,225,101,89,191,224,1001,224,-100,224,4,224,1002,223,8,223,1001,224,1,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,8,226,677,224,1002,223,2,223,1006,224,329,1001,223,1,223,108,226,226,224,1002,223,2,223,1006,224,344,1001,223,1,223,7,677,226,224,102,2,223,223,1005,224,359,101,1,223,223,7,226,677,224,102,2,223,223,1006,224,374,101,1,223,223,1008,677,226,224,1002,223,2,223,1006,224,389,101,1,223,223,8,677,677,224,1002,223,2,223,1005,224,404,101,1,223,223,8,677,226,224,102,2,223,223,1005,224,419,1001,223,1,223,1107,677,226,224,102,2,223,223,1005,224,434,1001,223,1,223,1107,226,677,224,1002,223,2,223,1006,224,449,1001,223,1,223,107,677,226,224,1002,223,2,223,1005,224,464,1001,223,1,223,1008,677,677,224,1002,223,2,223,1006,224,479,1001,223,1,223,1108,677,226,224,1002,223,2,223,1006,224,494,1001,223,1,223,1108,677,677,224,1002,223,2,223,1006,224,509,1001,223,1,223,107,677,677,224,1002,223,2,223,1005,224,524,101,1,223,223,1007,677,226,224,102,2,223,223,1005,224,539,1001,223,1,223,1107,226,226,224,1002,223,2,223,1006,224,554,1001,223,1,223,1008,226,226,224,1002,223,2,223,1006,224,569,101,1,223,223,1108,226,677,224,1002,223,2,223,1006,224,584,101,1,223,223,108,677,677,224,1002,223,2,223,1006,224,599,1001,223,1,223,1007,677,677,224,102,2,223,223,1006,224,614,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,629,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,644,1001,223,1,223,108,226,677,224,102,2,223,223,1005,224,659,1001,223,1,223,7,677,677,224,102,2,223,223,1006,224,674,1001,223,1,223,4,223,99,226]
  @test [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]

  def part1(), do: execute_intcode(@instructions, 1)
  def part2(), do: execute_intcode(@instructions, 5)
  def test(number), do: execute_intcode(@test, number)

  defp execute_intcode(instructions, input) do
    exec(instructions, [input], 0)
  end

  defp exec(instructions, output, position) when position == length(instructions) do
    output
  end
  defp exec(instructions, output, position) do
    # IO.puts(" ")
    # IO.inspect(instructions |> List.replace_at(position, [Enum.at(instructions, position)]), charlists: :as_lists, pretty: :true, label: :instructions)
    # IO.inspect(output, label: :output)
    # IO.puts("Position is now #{position}")
    current = Enum.at(instructions, position)
    case operation_int(current) do
      :terminate ->
        output
      :add ->
        add(instructions, output, position, [{:position, Enum.at(instructions, position + 1)}, {:position, Enum.at(instructions, position + 2)}, {:position, Enum.at(instructions, position + 3)}])
      :multiply ->
        multiply(instructions, output, position, [{:position, Enum.at(instructions, position + 1)}, {:position, Enum.at(instructions, position + 2)}, {:position, Enum.at(instructions, position + 3)}])
      :store ->
        store(instructions, [], position, [{:position, Enum.at(output, 0)}])
      :output ->
        output(instructions, output, position, [{:position, Enum.at(instructions, position + 1)}])
      :jump_if_true ->
        jump_if_true(instructions, output, position, [{:position, Enum.at(instructions, position + 1)}, {:position, Enum.at(instructions, position + 2)}])
      :jump_if_false ->
        jump_if_false(instructions, output, position, [{:position, Enum.at(instructions, position + 1)}, {:position, Enum.at(instructions, position + 2)}])
      :less_than ->
        less_than(instructions, output, position, [{:position, Enum.at(instructions, position + 1)}, {:position, Enum.at(instructions, position + 2)}, {:position, Enum.at(instructions, position + 3)}])
      :equals ->
        equals(instructions, output, position, [{:position, Enum.at(instructions, position + 1)}, {:position, Enum.at(instructions, position + 2)}, {:position, Enum.at(instructions, position + 3)}])
      :other ->
        curr_reverse =
          current
          |> Kernel.to_string()
          |> String.reverse()
          |> String.split("", trim: true)

        IO.inspect(curr_reverse, label: :curr_reverse)

        opcode =
          Enum.at(curr_reverse, 1) <> Enum.at(curr_reverse, 0)
          |> Integer.parse()
          |> elem(0)
          |> IO.inspect(label: :opcode)
          |> operation_int()

        mode1 = if Enum.count(curr_reverse) >= 3 do
            param_mode_int(Integer.parse(Enum.at(curr_reverse, 2)) |> elem(0))
          else
            param_mode_int(0)
          end

          mode2 = if Enum.count(curr_reverse) >= 4 do
            param_mode_int(Integer.parse(Enum.at(curr_reverse, 3)) |> elem(0))
          else
            param_mode_int(0)
          end

          mode3 = if Enum.count(curr_reverse) >= 5 do
            param_mode_int(Integer.parse(Enum.at(curr_reverse, 4)) |> elem(0))
          else
            param_mode_int(0)
          end

        IO.inspect(%{current: current, opcode: opcode, mode1: mode1, mode2: mode2, mode3: mode3})

        case opcode do
          :add ->
            add(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}, {mode2, Enum.at(instructions, position + 2)}, {mode3, Enum.at(instructions, position + 3)}])
          :multiply ->
            multiply(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}, {mode2, Enum.at(instructions, position + 2)}, {mode3, Enum.at(instructions, position + 3)}])
          :store ->
            store(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}])
          :output ->
            output(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}])
          :jump_if_true ->
            jump_if_true(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}, {mode2, Enum.at(instructions, position + 2)}])
          :jump_if_false ->
            jump_if_false(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}, {mode2, Enum.at(instructions, position + 2)}])
          :less_than ->
            less_than(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}, {mode2, Enum.at(instructions, position + 2)}, {mode3, Enum.at(instructions, position + 3)}])
          :equals ->
            equals(instructions, output, position, [{mode1, Enum.at(instructions, position + 1)}, {mode2, Enum.at(instructions, position + 2)}, {mode3, Enum.at(instructions, position + 3)}])
          unknown ->
            raise("Unknown opcode #{unknown}")
        end
    end
  end

  defp getopval(:position, val, instructions) do
    result = Enum.at(instructions, val)
    IO.puts("Arg #{val} by position is #{result}")
    result
  end
  defp getopval(:immediate, val, _instructions) do
    IO.puts("Arg #{val} by immediate is #{val}")
    val
  end


  # Opcode 1 - Add
  defp add(instructions, output, position, [{mode1, arg1}, {mode2, arg2}, {mode3, arg3}]) do
    opval1 = getopval(mode1, arg1, instructions)
    opval2 = getopval(mode2, arg2, instructions)

    result = opval1 + opval2

    case mode3 do
      :position ->
        exec(List.replace_at(instructions, arg3, result), output, position + 4)
      :immediate ->
        exec(instructions, output ++ [result], position + 4)
    end
  end
  # Opcode 2 - Multiply
  defp multiply(instructions, output, position, [{mode1, arg1}, {mode2, arg2}, {mode3, arg3}]) do
    opval1 = getopval(mode1, arg1, instructions)
    opval2 = getopval(mode2, arg2, instructions)

    result = opval1 * opval2

    case mode3 do
      :position ->
        exec(List.replace_at(instructions, arg3, result), output, position + 4)
      :immediate ->
        exec(instructions, output ++ [result], position + 4)
    end
  end
  # Opcode 3 - Store
  # Opcode 3 takes a single integer as input and saves it to the position given by its only parameter.
  # For example, the instruction 3,50 would take an input value and store it at address 50.
  defp store(instructions, output, position, [{mode1, arg1}]) do
    to_store = case mode1 do
      :position ->
        Enum.at(instructions, position + 1)
      :immediate ->
        raise("Got :immediate mode while attempting to store")
    end
    instructions = List.replace_at(instructions, to_store, arg1)
    exec(instructions, output, position + 2)
  end
  # Opcode 4 - Output
  # Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50 would output
  # the value at address 50.
  defp output(instructions, output, position, [{mode1, arg1}]) do
    output_val = arg_for_mode(instructions, mode1, arg1)

    exec(instructions, output ++ [output_val], position + 2)
  end
  # Opcode 5 - jump-if-true
  # if the first parameter is non-zero, it sets the instruction pointer to the value from the second parameter.
  # Otherwise, it does nothing.
  defp jump_if_true(instructions, output, position, [{mode1, arg1}, {mode2, arg2}]) do
    jump_switch = arg_for_mode(instructions, mode1, arg1)

    if jump_switch == 0 do
      exec(instructions, output, position + 3)
    else
      jump_point = case mode2 do
        :position ->
          Enum.at(instructions, arg2)
        :immediate ->
          arg2
      end
      exec(instructions, output, jump_point)
    end
  end
  # Opcode 6 - jump-if-false
  # if the first parameter is zero, it sets the instruction pointer to the value from the second parameter.
  # Otherwise, it does nothing.
  defp jump_if_false(instructions, output, position, [{mode1, arg1}, {mode2, arg2}]) do
    jump_switch = arg_for_mode(instructions, mode1, arg1)

    if jump_switch != 0 do
      exec(instructions, output, position + 3)
    else
      jump_point = case mode2 do
        :position ->
          Enum.at(instructions, arg2)
        :immediate ->
          arg2
      end
      exec(instructions, output, jump_point)
    end
  end
  # Opcode 7 - less than
  # if the first parameter is less than the second parameter, it stores 1 in the position given by the third parameter.
  # Otherwise, it stores 0.
  defp less_than(instructions, output, position, [{mode1, arg1}, {mode2, arg2}, {_mode3, arg3}]) do
    left = arg_for_mode(instructions, mode1, arg1)
    right = arg_for_mode(instructions, mode2, arg2)
    store_pos = arg3 # TODO: Investigate ambiguity in puzzle description

    instructions
    |> List.replace_at(store_pos, bool_to_int(left < right))
    |> exec(output, position + 4)
  end
  # Opcode 8 - equals
  # if the first parameter is equal to the second parameter, it stores 1 in the position given by the third parameter.
  # Otherwise, it stores 0.
  defp equals(instructions, output, position, [{mode1, arg1}, {mode2, arg2}, {_mode3, arg3}]) do
    left = arg_for_mode(instructions, mode1, arg1)
    right = arg_for_mode(instructions, mode2, arg2)
    store_pos = arg3 # TODO: Investigate ambiguity in puzzle description

    instructions
    |> List.replace_at(store_pos, bool_to_int(left == right))
    |> exec(output, position + 4)
  end

  defp bool_to_int(true), do: 1
  defp bool_to_int(false), do: 0

  defp arg_for_mode(instructions, :position, arg), do: Enum.at(instructions, arg)
  defp arg_for_mode(_instructions, :immediate, arg), do: arg

  defp operation_int(1), do: :add
  defp operation_int(2), do: :multiply
  defp operation_int(3), do: :store
  defp operation_int(4), do: :output
  defp operation_int(5), do: :jump_if_true
  defp operation_int(6), do: :jump_if_false
  defp operation_int(7), do: :less_than
  defp operation_int(8), do: :equals
  defp operation_int(99), do: :terminate
  defp operation_int(_), do: :other

  defp param_mode_int(0), do: :position
  defp param_mode_int(1), do: :immediate
end
