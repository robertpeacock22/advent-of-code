defmodule Y2019.Day02 do
  def part1() do
    input = [1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,9,1,19,1,5,19,23,2,9,23,27,1,27,5,31,2,31,13,35,1,35,9,39,1,39,10,43,2,43,9,47,1,47,5,51,2,13,51,55,1,9,55,59,1,5,59,63,2,6,63,67,1,5,67,71,1,6,71,75,2,9,75,79,1,79,13,83,1,83,13,87,1,87,5,91,1,6,91,95,2,95,13,99,2,13,99,103,1,5,103,107,1,107,10,111,1,111,13,115,1,10,115,119,1,9,119,123,2,6,123,127,1,5,127,131,2,6,131,135,1,135,2,139,1,139,9,0,99,2,14,0,0]
    execute_intcode(input)
  end

  def part2() do
    desired_result = 19690720
    input = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,9,1,19,1,5,19,23,2,9,23,27,1,27,5,31,2,31,13,35,1,35,9,39,1,39,10,43,2,43,9,47,1,47,5,51,2,13,51,55,1,9,55,59,1,5,59,63,2,6,63,67,1,5,67,71,1,6,71,75,2,9,75,79,1,79,13,83,1,83,13,87,1,87,5,91,1,6,91,95,2,95,13,99,2,13,99,103,1,5,103,107,1,107,10,111,1,111,13,115,1,10,115,119,1,9,119,123,2,6,123,127,1,5,127,131,2,6,131,135,1,135,2,139,1,139,9,0,99,2,14,0,0]

    0..140
    |> Enum.reduce({0, 0}, fn noun_position, acc ->
      if acc != {0, 0} do
        acc
      else
        0..140
        |> Enum.reduce({0, 0}, fn verb_position, acc ->
          if acc != {0, 0} do
            acc
          else
            result =
              input
              |> List.replace_at(1, noun_position)
              |> List.replace_at(2, verb_position)
              |> execute_intcode()

            if Enum.at(result, 0) == desired_result do
              {noun_position, verb_position}
            else
              {0, 0}
            end
          end
        end)
      end
    end)
  end

  def execute_intcode(array_of_ints) do
    array_of_ints
    |> Enum.reduce(
      %{array: array_of_ints, position: 0, upcoming: %{}},
      fn _array, acc ->
        case rem(acc.position, 4) do
          0 ->
            operation = acc.array |> Enum.at(acc.position) |> operation_int()
            new_upcoming = acc.upcoming |> Map.put(:operation, operation)
            %{acc | upcoming: new_upcoming, position: acc.position + 1}
          1 ->
            pos = acc.array |> Enum.at(acc.position)
            lhs = acc.array |> Enum.at(pos)
            new_upcoming = acc.upcoming |> Map.put(:lhs, lhs)
            %{acc | upcoming: new_upcoming, position: acc.position + 1}
          2 ->
            pos = acc.array |> Enum.at(acc.position)
            rhs = acc.array |> Enum.at(pos)
            new_upcoming = acc.upcoming |> Map.put(:rhs, rhs)
            %{acc | upcoming: new_upcoming, position: acc.position + 1}
          3 ->
            position = acc.array |> Enum.at(acc.position)
            case acc.upcoming.operation do
              :terminate -> acc
              :add ->
                result = acc.upcoming.lhs + acc.upcoming.rhs
                mod_array = acc.array |> List.replace_at(position, result)
                %{acc | array: mod_array, upcoming: %{}, position: acc.position + 1}
              :multiply ->
                result = acc.upcoming.lhs * acc.upcoming.rhs
                mod_array = acc.array |> List.replace_at(position, result)
                %{acc | array: mod_array, upcoming: %{}, position: acc.position + 1}
              _ -> raise "invalid operation code"
            end
        end
      end)
      |> Map.get(:array)
    end

  defp operation_int(1), do: :add
  defp operation_int(2), do: :multiply
  defp operation_int(99), do: :terminate
  defp operation_int(input), do: raise "invalid operation int #{input}"
end
