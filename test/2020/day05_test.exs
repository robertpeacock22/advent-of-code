defmodule Y2020.Day05Test do
  use ExUnit.Case
  alias Y2020.Day05

  test "BFFFBBFRRR" do
    # row 70, column 7, seat ID 567
    assert Day05.parse_pass("BFFFBBFRRR") == %{row: 70, col: 7, seat: 567}
  end

  test "FFFBBBFRRR" do
    # row 14, column 7, seat ID 119
    assert Day05.parse_pass("FFFBBBFRRR") == %{row: 14, col: 7, seat: 119}
  end

  test "BBFFBBFRLL" do
    # row 102, column 4, seat ID 820
    assert Day05.parse_pass("BBFFBBFRLL") == %{row: 102, col: 4, seat: 820}
  end
end
