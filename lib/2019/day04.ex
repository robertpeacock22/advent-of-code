defmodule Y2019.Day04 do

  @range_bottom 156218
  @range_top 652527

  def part1() do
    @range_bottom..@range_top
    |> Enum.reject(fn number ->
      number
      |> Integer.digits()
      |> Enum.reduce({[], false}, fn digit, {digits, rule_broken?} ->
        if rule_broken? do
          {digits, rule_broken?}
        else
          rule_broken? =
            cond do
              length(digits) == 0 ->
                false
              digit < List.last(digits) ->
                true
              true ->
                false
            end

          {digits ++ [digit], rule_broken?}
        end
      end)
      |> elem(1)
    end)
    |> Enum.filter(fn number ->
      number
      |> Integer.digits()
      |> Enum.reduce({[], false}, fn digit, {digits, rule_satisfied?} ->
        if rule_satisfied? do
          {digits, rule_satisfied?}
        else
          rule_satisfied? =
            cond do
              length(digits) == 0 ->
                false
              digit == List.last(digits) ->
                true
              true ->
                false
          end

          {digits ++ [digit], rule_satisfied?}
        end
      end)
      |> elem(1)
    end)
    |> length()
  end

  def part2() do
    @range_bottom..@range_top
    |> Enum.reject(fn number ->
      number
      |> Integer.digits()
      |> Enum.reduce({[], false}, fn digit, {digits, rule_broken?} ->
        if rule_broken? do
          {digits, rule_broken?}
        else
          rule_broken? =
            cond do
              length(digits) == 0 ->
                false
              digit < List.last(digits) ->
                true
              true ->
                false
            end

          {digits ++ [digit], rule_broken?}
        end
      end)
      |> elem(1)
    end)
    |> Enum.filter(fn number ->
      number
      |> Integer.digits()
      |> Enum.reverse()
      |> Enum.chunk_by(&(&1))
      |> Enum.reduce({[], false}, fn digit_set, {digit_sets, rule_satisfied?} ->
        if rule_satisfied? do
          {digit_sets, rule_satisfied?}
        else
          rule_satisfied? =
            cond do
              length(digit_set) == 2 ->
                true
              true ->
                false
            end

          {digit_sets ++ [digit_set], rule_satisfied?}
        end
      end)
      |> elem(1)
    end)
    |> length()
  end
end
